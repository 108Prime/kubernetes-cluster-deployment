resource "google_compute_network" "vpc_network" {
  name                    = var.vpc_name
  auto_create_subnetworks = var.auto_create_subnetworks
}

resource "google_compute_subnetwork" "private_subnet" {
  name          = var.private_subnet_name
  region        = var.region
  network       = google_compute_network.vpc_network.self_link
  ip_cidr_range = var.private_subnet_cidr_range
}

resource "google_compute_subnetwork" "public_subnet" {
  name          = var.public_subnet_name
  region        = var.region
  network       = google_compute_network.vpc_network.self_link
  ip_cidr_range = var.public_subnet_cidr_range
}

output "vpc_name" {
  value = google_compute_network.vpc_network.name
}

output "private_subnet_name" {
  value = google_compute_subnetwork.private_subnet.name
}
