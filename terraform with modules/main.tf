terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("/home/jagmohan/Coding Jindagi/terraform/ardent-gearbox-389009-7769fd817761.json")

  project = "ardent-gearbox-389009"
  region  = "us-central1"
  zone    = "us-central1-c"
}

module "vpc" {
  source = "./modules/vpc"

  vpc_name                   = var.vpc_name
  auto_create_subnetworks    = var.auto_create_subnetworks
  private_subnet_name        = var.private_subnet_name
  private_subnet_cidr_range  = var.private_subnet_cidr_range
  public_subnet_name         = var.public_subnet_name
  public_subnet_cidr_range   = var.public_subnet_cidr_range
}

module "instance" {
  source = "./modules/instance"

  instance_name  = var.instance_name
  machine_type   = var.machine_type
  zone           = var.zone
  image          = var.image
  subnetwork     = module.vpc.public_subnet_name
  network        = module.vpc.vpc_name
}

