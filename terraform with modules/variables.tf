variable "vpc_name" {
  description = "Name of the VPC network"
  type        = string
}

variable "auto_create_subnetworks" {
  description = "Whether to automatically create subnetworks"
  type        = bool
  default     = false
}

variable "private_subnet_name" {
  description = "Name of the private subnet"
  type        = string
}

variable "private_subnet_cidr_range" {
  description = "CIDR range for the private subnet"
  type        = string
}

variable "public_subnet_name" {
  description = "Name of the public subnet"
  type        = string
}

variable "public_subnet_cidr_range" {
  description = "CIDR range for the public subnet"
  type        = string
}

variable "instance_name" {
  description = "Name of the compute instance"
  type        = string
}

variable "machine_type" {
  description = "Machine type of the compute instance(for ex. e2-micro , e2-small , e2-medium"
  type        = string
}

variable "zone" {
  description = "Zone for the compute instance"
  type        = string
}

variable "image" {
  description = "Image for the compute instance"
  type        = string
  default     = "debian-cloud/debian-10"
}

