#!/bin/bash

# Array of ConfigMap names
CONFIGMAP_NAMES=("mongodb-configmap")

# Set the namespace
#NAMESPACE="your-namespace"

# Generate a new password
NEW_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)

# Iterate over the ConfigMap names
for CONFIGMAP_NAME in "${CONFIGMAP_NAMES[@]}"; do
  # Retrieve the current ConfigMap YAML
  CURRENT_CONFIGMAP=$(kubectl get configmap "$CONFIGMAP_NAME" -o yaml)

  # Update the password in the ConfigMap YAML
  UPDATED_CONFIGMAP=$(echo "$CURRENT_CONFIGMAP" | sed "s/giit123/$NEW_PASSWORD/g")

  # Create a temporary file to store the updated ConfigMap YAML
  TMP_FILE=$(mktemp)

  # Save the updated ConfigMap YAML to the temporary file
  echo "$UPDATED_CONFIGMAP" > "$TMP_FILE"

  # Apply the updated ConfigMap YAML
  kubectl apply -f "$TMP_FILE" -n "$NAMESPACE"

  # Clean up the temporary file
  rm "$TMP_FILE"

  # Print a success message for each ConfigMap
  echo "Password rotated successfully for ConfigMap: $CONFIGMAP_NAME"
done

# Print a final success message
echo "Password rotation completed for all ConfigMaps."

