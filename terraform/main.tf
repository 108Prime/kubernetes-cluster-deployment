terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("/home/jagmohan/Coding Jindagi/terraform/ardent-gearbox-389009-7769fd817761.json")

  project = "ardent-gearbox-389009"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_network" "vpc_network" {
  name                    = "my-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "private_subnet" {
  name          = "private-subnet"
  region        = "us-central1"
  network       = google_compute_network.vpc_network.self_link
  ip_cidr_range = "10.0.1.0/24"
}

resource "google_compute_subnetwork" "public_subnet" {
  name          = "public-subnet"
  region        = "us-central1"
  network       = google_compute_network.vpc_network.self_link
  ip_cidr_range = "10.0.2.0/24"
}
resource "google_compute_instance" "first_instance" {
  count        = 3
  name         = "vm-prod-${count.index}"
  machine_type = "f1-micro"
  zone         = "us-central1-c"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10" 
    }
  }

  network_interface {
    subnetwork= google_compute_subnetwork.private_subnet.name
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}
